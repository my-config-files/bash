# Yann Vote's personal .bashrc
#
# Copyright (c) 2015 Yann Vote <yann.vote@gmx.com>
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
# OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
# PERFORMANCE OF THIS SOFTWARE.

# (From Arch Linux) If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Global settings
if [ -r /etc/bashrc ]; then
        . /etc/bashrc
fi

if [ -r /etc/bash.bashrc ]; then
        . /etc/bash.bashrc
fi

# Variables
LOCAL_CONF_DIR=~/.config/bash
export PATH=$HOME/.local/bin:${PATH}
export EDITOR=vim
export HISTCONTROL=ignoreboth:erasedups

# User aliases
if [ -s ~/.aliases ]; then
        . ~/.aliases
fi

# Host specific aliases
for file in `ls $LOCAL_CONF_DIR/*.aliases`; do
        . $file
done

# Host specific settings
for file in `ls $LOCAL_CONF_DIR/*.bashrc`; do
        . $file
done

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/opt/miniforge3/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/opt/miniforge3/etc/profile.d/conda.sh" ]; then
        . "/opt/miniforge3/etc/profile.d/conda.sh"
    else
        export PATH="/opt/miniforge3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<
